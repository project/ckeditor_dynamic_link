<?php

namespace Drupal\ckeditor_dynamic_link\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Class DynamicLinkManager.
 *
 * @package Drupal\ckeditor_dynamic_link\Service
 */
class CKEditorDynamicLinkManager {

  const SERVICE_NAME = 'ckeditor_dynamic_link.manager';

  const TABLE = 'ckeditor_dynamic_link_usage';

  const FIELD_ENTITY_ID = 'entity_id';

  const FIELD_ENTITY_TYPE = 'entity_type';

  const FIELD_REFERER_ID = 'referer_id';

  const FIELD_REFERER_TYPE = 'referer_type';

  protected $currentNode = FALSE;

  /**
   * EntityType manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity Repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * DynamicLinkManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityRepositoryInterface $entityRepository) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityRepository = $entityRepository;
  }

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Ajoute un référetant à l'entité linkée.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\EntityInterface $referer
   *   The referer.
   */
  public function addReferer(EntityInterface $entity = NULL, EntityInterface $referer = NULL) {

    if ($entity && $referer) {
      $data = [
        static::FIELD_ENTITY_ID    => $entity->id(),
        static::FIELD_ENTITY_TYPE  => $entity->getEntityTypeId(),
        static::FIELD_REFERER_ID   => $referer->id(),
        static::FIELD_REFERER_TYPE => $referer->getEntityTypeId(),
      ];

      try {
        \Drupal::database()->insert(static::TABLE)
          ->fields(array_keys($data))
          ->values($data)
          ->execute();
      }
      catch (\Exception $e) {
        // Mute exception...
        // En fait ici, on arrive si l'élément est déjà en db.
        // je pense qu'il est moins couteux de gérer l'exception
        // Plutot que de faire une requête de test puis une requete d'insertion.
      }
    }
  }

  /**
   * Retourne l'entité depuis l'id passé en attribut.
   *
   * @param string $id
   *   L'id (ex:  'node:3').
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   L'entité.
   */
  public function getEntityFromId($id) {
    list($type, $id) = explode(':', $id);

    try {
      if ($entity = $this->entityTypeManager->getStorage($type)->load($id)) {
        return $this->entityRepository->getTranslationFromContext($entity);
      }
    }
    catch (\Exception $e) {
      // Mute exception...
    }

    return NULL;
  }

  /**
   * Action à la modification de path d'un élément.
   *
   * @param array $path
   *   The path.
   */
  public function onPathUpdate(array $path = []) {
    try {
      list($entityType, $entityId) = explode(':', $this->getIdByPath($path));
      $refererIds = $this->getRefererIds($entityType, $entityId);
      Cache::invalidateTags($refererIds);
    }
    catch (\Exception $e) {
      // Mute exception...
    }

  }

  /**
   * Retourne l'id (node:3) depuis le path.
   *
   * @param array $path
   *   The path.
   *
   * @return string
   *   The id.
   */
  public function getIdByPath(array $path = []) {
    list($null, $type, $id) = explode('/', $path['source']);

    return $type . ':' . $id;
  }

  /**
   * Retourne la liste des ids des referer.
   *
   * @param string $entityType
   *   The type.
   * @param string $entityId
   *   The id.
   *
   * @return array
   *   The ids list.
   */
  public function getRefererIds($entityType, $entityId) {
    $query = \Drupal::database()->select(static::TABLE, 't');
    $query->addExpression('CONCAT(' . static::FIELD_REFERER_TYPE . ', \':\', ' . static::FIELD_REFERER_ID . ')', 'result');
    $query->condition(static::FIELD_ENTITY_TYPE, $entityType);
    $query->condition(static::FIELD_ENTITY_ID, $entityId);

    try {
      return $query->execute()->fetchCol();
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * @return false
   */
  public function getCurrentPageNode() {
    if ($this->currentNode === FALSE) {
      if ($node = \Drupal::routeMatch()->getParameter('node')) {
        if( is_numeric($node) ){
          $this->currentNode = $this->getEntityFromId('node:'.$node);
        }
        else {
          $this->currentNode = $this->entityRepository->getTranslationFromContext($node);
        }
      }
    }

    if( $this->currentNode === FALSE ){
      $this->currentNode = null;
    }

    return $this->currentNode;
  }

}
