<?php

namespace Drupal\ckeditor_dynamic_link\EditorXssFilter;

use Drupal\ckeditor_dynamic_link\Form\CKEditorDynamicLinkDialog;
use Drupal\editor\EditorXssFilter\Standard;
use Drupal\filter\FilterFormatInterface;

/**
 * Defines the standard text editor XSS filter.
 */
class DynamicLinkXssFilter extends Standard {

  /**
   * {@inheritdoc}
   */
  public static function filterXss($html, FilterFormatInterface $format, FilterFormatInterface $original_format = NULL) {
    $replacement = [];
    $html = static::tmpReplaceShortcodes($html, CKEditorDynamicLinkDialog::SHORTCODE_ID, $replacement);
    $html = parent::filterXss($html, $format, $original_format);
    $html = static::reInitTokens($html, $replacement);

    return $html;
  }

  /**
   * Temporary replace shortcodes with xss allowed values.
   *
   * @param string $html
   *   The initial html.
   * @param string $token
   *   THe token id.
   * @param array $replacement
   *   The replacement array.
   *
   * @return array|string|string[]
   *   The replaced values.
   */
  private static function tmpReplaceShortcodes(string $html, $token, &$replacement) {
    // Get all shorcode values in href attributes.
    preg_match('/(?<=href=\"\[' . $token . ' id=\')(.*)(?=\'\]\[\/' . $token . '\])/', $html, $matches);

    // Generate replacement array.
    foreach(array_unique($matches) as $match){
      $replacement["[" . $token . " id='" . $match . "'][/" . $token . "]"] = '///'. str_replace(':','/', $match);
    }

    return str_replace(array_keys($replacement), array_values($replacement), $html);
  }

  /**
   * Place the token in the initial string.s
   *
   * @param string $html
   *   The initial markup.
   * @param array $replacement
   *   The replacement array.
   *
   * @return array|string|string[]
   *   The replaced value.
   */
  private static function reInitTokens(string $html, array $replacement) {
    return str_replace(array_values($replacement), array_keys($replacement), $html);
  }

}
