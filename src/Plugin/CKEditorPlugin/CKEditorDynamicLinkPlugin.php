<?php

namespace Drupal\ckeditor_dynamic_link\Plugin\CKEditorPlugin;

use Drupal\ckeditor_entity_link\Plugin\CKEditorPlugin\EntityLink;

/**
 * Defines the "ckeditor_dynamic_link" plugin.
 *
 * @CKEditorPlugin(
 *   id = "ckeditor_dynamic_link",
 *   label = @Translation("CKEditor Dynamic Link"),
 * )
 */
class CKEditorDynamicLinkPlugin extends EntityLink {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_dynamic_link') . '/js/plugins/ckeditordynamiclink/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = drupal_get_path('module', 'ckeditor_dynamic_link') . '/js/plugins/ckeditordynamiclink';
    return array(
      'DynamicLink' => array(
        'label' => t('CKEditor Dynamic Link'),
        'image' => $path . '/ckeditor_dynamic_link.png',
      ),
    );
  }

}
