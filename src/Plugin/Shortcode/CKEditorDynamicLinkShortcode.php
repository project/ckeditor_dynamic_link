<?php

namespace Drupal\ckeditor_dynamic_link\Plugin\Shortcode;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;
use Drupal\ckeditor_dynamic_link\Service\CKEditorDynamicLinkManager;

/**
 * Provides a shortcode for bootstrap columns.
 *
 * @Shortcode(
 *   id = "ckedl",
 *   title = @Translation("CKEditor Dynamic link shortcode"),
 *   description = @Translation("Display a url from token.")
 * )
 */
class CKEditorDynamicLinkShortcode extends ShortcodeBase {

  /**
   * @var \Drupal\ckeditor_dynamic_link\Service\CKEditorDynamicLinkManager
   */
  protected $dynamicLinkManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dynamicLinkManager = CKEditorDynamicLinkManager::me();
  }



  /**
   * {@inheritdoc}
   */
  public function process(array $attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    if (array_key_exists('id', $attributes) && !empty($attributes['id'])) {
      if ($url = $this->getUrlFromId($attributes['id'])) {
        return $url;
      }
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return ' ';
  }

  /**
   * Retourne  l'url depuis l'id passé en paramètre.
   *
   * @param string $id
   *   L'id (ex:  'node:3').
   *
   * @return \Drupal\Core\GeneratedUrl|null|string
   *   L'url.
   */
  protected function getUrlFromId($id) {
    /** @var EntityInterface $entity */
    if ($entity = $this->getEntityFromId($id)) {
      $this->addReferer($entity);
      return $entity->toUrl()->toString();
    }
    return NULL;
  }

  /**
   * Retourne l'entité depuis l'id passé en attribut.
   *
   * @param string $id
   *   L'id (ex:  'node:3').
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   L'entité.
   */
  protected function getEntityFromId($id) {
    return $this->dynamicLinkManager->getEntityFromId($id);
  }

  /**
   * Ajoute un referer.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  protected function addReferer(EntityInterface $entity) {
    $this->dynamicLinkManager->addReferer($entity, $this->dynamicLinkManager->getCurrentPageNode());
  }

}
