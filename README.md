CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers


INTRODUCTION
------------
The CKEditor Dynamic Link module enhances link popup with a dynamic reference to the target.


REQUIREMENTS
------------

- drupal:shortcode
- drupal:ckeditor_entity_link


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


MAINTAINERS
-----------

Current maintainers:
* Thomas Sécher (tsecher) - https://www.drupal.org/u/tsecher


Supporting organization:
* Conserto - https://conserto.pro/
